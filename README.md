# Order Book

https://kendall-july-22-2021.vercel.app/

# Folder Structure

_All source code lives within `src/`_

- `pages/` is a Next.js [concept](https://nextjs.org/docs/basic-features/pages) https://nextjs.org/docs/basic-features/pages
- `scenes/` are compositions of components
- `shared/`
  - `components/` are re-usable components
  - `enums/` are re-usable enums
  - `helpers/` are re-usable helper functions

### _Modlet_ Concept

"A modlet is a self-contained React component."

Modlet's can be composed of:

- Itself
  - Exported via an `index.ts` file
  - Test file
  - Style file
  - Component file
- Nested components (`components/`)
  - **Note:** these nested components can only be used by the parent modlet. If a component needs to be sharable across modlets, then it belongs in the root `shared/` folder.
- Enums (`enums/`)
- Helpers (`helpers/`)
  - Tests
- Hooks (`hooks/`)
  - Tests
- Interfaces (`interfaces/`)

Within this project, the `scenes/` & `shared/components/` folders are both composed of modlets.

# Random Thoughts

- Would be nice to style in accordance w/ co. styles
- Loading animation/state would be nice
- Dev server styled components bug solution: https://github.com/vercel/next.js/issues/7322#issuecomment-846429604
- Probably should've used the native `<table>` tags but meh
- Didn't get full test coverage on components b/c crunched for time, but proved the point

import { NextPage } from "next";
import OrderBook from "@scenes/OrderBook";

const Home: NextPage<{}> = () => {
  return <OrderBook />;
};

export default Home;

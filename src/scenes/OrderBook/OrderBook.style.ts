import styled from "styled-components";

export const Layout = styled.div`
  width: 70%;
  margin: auto;
  background-color: #0e0c28;
  min-height: 550px;
  display: flex;
  flex-direction: column;
`;

export const Header = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 16px;
`;

export const Book = styled.div`
  flex-grow: 1;
  display: flex;

  @media only screen and (max-width: 991px) {
    flex-direction: column-reverse;
  }
`;

export const Table = styled.div`
  flex: 1;
`;

export const Buttons = styled.div`
  width: 35%;
  margin: auto;
  display: flex;
  justify-content: space-between;
  padding: 16px;
`;

import { shallow } from "enzyme";
import { expect } from "chai";
import OrderBook from "./OrderBook";
import * as hooks from "./hooks/useOrderBook";
import ProductId from "./enums/ProductId";
import OrderTable from "./components/OrderTable";
import Error from "./components/Error";

let state = {
  asks: [],
  bids: [],
  asset: ProductId.PI_XBTUSD,
  onClickToggleFeed: () => {},
  onChangeGrouping: () => {},
  onClickKillFeed: () => {},
  isErrored: false,
  spread: 0,
  spreadPercentage: 0,
};

describe("<OrderBook />", () => {
  describe("Initial state", () => {
    it("Renders empty state", () => {
      jest.spyOn(hooks, "default").mockImplementation(() => state);

      const component = shallow(<OrderBook />);

      expect(component.find(OrderTable)).to.have.lengthOf(2);
    });
  });

  describe("Error state", () => {
    it("Renders error message", () => {
      jest.spyOn(hooks, "default").mockImplementation(() => {
        return { ...state, isErrored: true };
      });

      const component = shallow(<OrderBook />);

      expect(component.find(Error)).to.have.lengthOf(1);
      expect(component.find(OrderTable)).to.have.lengthOf(0);
    });
  });

  describe("Successful data-flow state", () => {
    it("Renders order book table", () => {
      jest.spyOn(hooks, "default").mockImplementation(() => state);

      const component = shallow(<OrderBook />);

      expect(component.find(OrderTable)).to.have.lengthOf(2);
    });
  });
});

import { FC } from "react";
import Error from "./components/Error";
import Grouping from "./components/Grouping";
import OrderTable from "./components/OrderTable";
import useOrderBook from "./hooks/useOrderBook";
import Button from "./components/Button";
import Color from "@enums/Color";
import { Layout, Header, Book, Table, Buttons } from "./OrderBook.style";
import Text from "@components/Text";
import HideMobile from "@components/HideMobile";
import HideDesktop from "@components/HideDesktop";

const OrderBook: FC<{}> = () => {
  const state = useOrderBook();

  return (
    <Layout>
      <Header>
        <Text>Order Book</Text>
        <HideMobile>
          <Text color={Color.grey}>
            Spread {state.spread.toFixed(2)} (
            {state.spreadPercentage.toFixed(2)}
            %)
          </Text>
        </HideMobile>
        <Grouping asset={state.asset} onChange={state.onChangeGrouping} />
      </Header>
      <Book>
        {/* TODO this should probably be it's own component */}
        {state.isErrored ? (
          <Error />
        ) : (
          <>
            <Table style={{ flex: "1" }}>
              <OrderTable orders={state.bids} />
            </Table>
            <HideDesktop>
              <Text color={Color.grey} isCenter={true} padding="16px">
                Spread {state.spread.toFixed(2)} (
                {state.spreadPercentage.toFixed(2)}
                %)
              </Text>
            </HideDesktop>
            <Table style={{ flex: "1" }}>
              <OrderTable orders={state.asks} />
            </Table>
          </>
        )}
      </Book>
      <Buttons>
        <Button
          backgroundColor={Color.purple}
          onClick={state.onClickToggleFeed}
        >
          Toggle Feed
        </Button>
        <Button backgroundColor={Color.red} onClick={state.onClickKillFeed}>
          Kill Feed
        </Button>
      </Buttons>
    </Layout>
  );
};

export default OrderBook;

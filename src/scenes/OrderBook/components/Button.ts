import styled from "styled-components";
import Color from "@enums/Color";

const Button = styled.button<{ backgroundColor: Color }>`
  background-color: ${(props) => props.backgroundColor};
  width: 128px;
  max-width: 128px;
  padding: 8px;
  color: white;
  border: none;
  border-radius: 4px;
  cursor: pointer;
`;

export default Button;

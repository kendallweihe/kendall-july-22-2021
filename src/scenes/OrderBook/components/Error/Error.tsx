import Text from "@components/Text";
import { FC } from "react";

const Error: FC<{}> = () => {
  return (
    <Text isCenter={true} width="100%">
      Uh oh! An unexpected error has occurred!
    </Text>
  );
};

export default Error;

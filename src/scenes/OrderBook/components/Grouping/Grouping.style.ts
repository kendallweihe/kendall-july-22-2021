import styled from "styled-components";

export const Container = styled.div`
  display: flex;
`;

export const Select = styled.select`
  background: none;
  border: none;
  color: white;
`;

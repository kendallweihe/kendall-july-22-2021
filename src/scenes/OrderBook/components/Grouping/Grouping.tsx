import { FC } from "react";
import { Container, Select } from "./Grouping.style";
import GroupingEnum from "../../enums/Grouping";
import ProductId from "@scenes/OrderBook/enums/ProductId";
import Text from "@components/Text";

const Grouping: FC<{
  asset: ProductId;
  onChange: (grouping: GroupingEnum) => void;
}> = (props) => {
  return (
    <Container>
      <Text>Group</Text>
      <Select
        onChange={(event) => props.onChange(event.target.value as GroupingEnum)}
      >
        <option value={GroupingEnum.small}>
          {props.asset === ProductId.PI_XBTUSD ? "0.50" : "0.05"}
        </option>
        <option value={GroupingEnum.medium}>
          {props.asset === ProductId.PI_XBTUSD ? "1.00" : "0.10"}
        </option>
        <option value={GroupingEnum.large}>
          {props.asset === ProductId.PI_XBTUSD ? "2.50" : "0.25"}
        </option>
      </Select>
    </Container>
  );
};

export default Grouping;

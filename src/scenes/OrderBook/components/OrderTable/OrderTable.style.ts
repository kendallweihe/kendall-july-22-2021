import styled from "styled-components";
import OrderType from "@enums/OrderType";

export const Container = styled.div``;

export const Header = styled.div<{ orderType: OrderType }>`
  display: flex;

  @media only screen and (min-width: 992px) {
    ${(props) =>
      props.orderType === OrderType.ask && `flex-direction: row-reverse;`}
  }

  @media only screen and (max-width: 991px) {
    flex-direction: row-reverse;
  }
`;

export const Body = styled.div<{ orderType: OrderType }>`
  display: flex;
  flex-direction: column;

  @media only screen and (max-width: 991px) {
    ${(props) =>
      props.orderType === OrderType.ask && `flex-direction: column-reverse;`}
  }
`;

import Text from "@components/Text";
import Color from "@enums/Color";
import IOrder from "@scenes/OrderBook/interfaces/IOrder";
import { FC } from "react";
import OrderRow from "./components/OrderRow";
import { Body, Container, Header } from "./OrderTable.style";

const OrderTable: FC<{
  orders: Array<IOrder>;
}> = (props) => {
  return (
    <Container>
      <Header orderType={props.orders[0]?.type}>
        <Text width="33%" isCenter={true} color={Color.grey}>
          TOTAL
        </Text>
        <Text width="33%" isCenter={true} color={Color.grey}>
          SIZE
        </Text>
        <Text width="33%" isCenter={true} color={Color.grey}>
          PRICE
        </Text>
      </Header>
      <Body orderType={props.orders[0]?.type}>
        {props.orders.map((order, index) => {
          return <OrderRow key={index} {...order} />;
        })}
      </Body>
    </Container>
  );
};

export default OrderTable;

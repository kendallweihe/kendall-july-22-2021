import styled from "styled-components";
import OrderType from "@enums/OrderType";
import Color from "@enums/Color";

export const Container = styled.div<{
  orderType: OrderType;
  barColor: Color;
  barWidth: number;
}>`
  display: flex;
  justify-content: space-around;
  padding-top: 4px;
  padding-bottom: 4px;

  ${(props) =>
    props.orderType === OrderType.bid
      ? `background: linear-gradient(to right, transparent ${
          100 - props.barWidth
        }%, ${props.barColor} ${100 - props.barWidth}%);`
      : `background: linear-gradient(to left, transparent ${
          100 - props.barWidth
        }%, ${props.barColor} ${100 - props.barWidth}%);`}

  @media only screen and (min-width: 992px) {
    ${(props) =>
      props.orderType === OrderType.ask && `flex-direction: row-reverse;`}
  }

  @media only screen and (max-width: 991px) {
    flex-direction: row-reverse;

    ${(props) =>
      `background: linear-gradient(to left, transparent ${
        100 - props.barWidth
      }%, ${props.barColor} ${100 - props.barWidth}%);`}
  }
`;

import Text from "@components/Text";
import Color from "@enums/Color";
import OrderType from "@enums/OrderType";
import formatNumber from "@helpers/formatNumber";
import IOrder from "@scenes/OrderBook/interfaces/IOrder";
import { FC } from "react";
import { Container } from "./OrderRow.style";

const OrderRow: FC<IOrder> = (props) => {
  return (
    <Container
      orderType={props.type}
      barColor={props.type === OrderType.bid ? Color.dullGreen : Color.dullRed}
      barWidth={props.totalPercentage}
    >
      <Text width="33%" isCenter={true}>
        {formatNumber(props.total)}
      </Text>
      <Text width="33%" isCenter={true}>
        {formatNumber(props.size)}
      </Text>
      <Text
        width="33%"
        isCenter={true}
        color={props.type === OrderType.bid ? Color.green : Color.red}
      >
        {formatNumber(props.price, true)}
      </Text>
    </Container>
  );
};

export default OrderRow;

enum Event {
  subscribe = "subscribe",
  subscribed = "subscribed",
  unsubscribe = "unsubscribe",
  unsubscribed = "unsubscribed",
}

export default Event;

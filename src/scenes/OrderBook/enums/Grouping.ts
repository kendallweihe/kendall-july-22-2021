enum Grouping {
  small = "small",
  medium = "medium",
  large = "large",
}

export default Grouping;

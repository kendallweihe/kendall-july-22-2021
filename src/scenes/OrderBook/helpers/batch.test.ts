import { expect } from "chai";
import IEvent from "../interfaces/IEvent";
import batch from "./batch";

describe("batch", () => {
  it("should wait & batch", (done) => {
    // note: this test could be improved by using some sort of spy
    let events: Array<IEvent> = [];

    const callback = (newEvents: Array<IEvent>) =>
      (events = [...events, ...newEvents]);

    const handler = batch(callback, 200);

    const data: Array<IEvent> = [
      {
        data: "1",
      },
      {
        data: "2",
      },
      {
        data: "3",
      },
    ];

    data.forEach((d) => handler(d));

    setTimeout(() => {
      // TODO to be real clever, assert the timing
      expect(events).to.deep.equal(data);
      done();
    }, 300);
  });
});

import IEvent from "../interfaces/IEvent";

const batch = (
  func: (events: Array<IEvent>) => void,
  wait: number,
  interval: number = 100
): ((event: IEvent) => void) => {
  let elapsed = 0;
  setInterval(() => (elapsed = elapsed + interval), interval);

  let events: IEvent[] = [];
  let timeout: NodeJS.Timeout;

  return (event: IEvent) => {
    events = [...events, event];

    clearTimeout(timeout);
    timeout = setTimeout(() => {
      func(events);
      elapsed = 0;
      events = [];
    }, wait - elapsed);
  };
};

export default batch;

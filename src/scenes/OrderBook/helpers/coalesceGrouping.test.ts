import { expect } from "chai";
import Grouping from "../enums/Grouping";
import coalesceGrouping from "./coalesceGrouping";

const data: number[][] = [
  [1, 1],
  [2, 2],
  [3, 3],
  [4, 4],
  [5, 5],
];

describe("coalesceGrouping", () => {
  describe("Small grouping", () => {
    it("Should return input", () => {
      const grouped = coalesceGrouping(data, Grouping.small);

      expect(grouped).to.deep.equals(data);
    });
  });

  describe("Medium grouping", () => {
    it("Should group every two", () => {
      const grouped = coalesceGrouping(data, Grouping.medium);

      expect(grouped).to.deep.equals([
        [1, 3],
        [3, 7],
        [5, 5],
      ]);
    });
  });

  describe("Large grouping", () => {
    it("Should group every five", () => {
      const grouped = coalesceGrouping(data, Grouping.large);

      expect(grouped).to.deep.equals([[1, 15]]);
    });
  });
});

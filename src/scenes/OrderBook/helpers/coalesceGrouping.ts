import Grouping from "../enums/Grouping";

const coalesceGrouping = (data: number[][], grouping: Grouping): number[][] => {
  if (grouping === Grouping.small) return data;

  const modulus = grouping === Grouping.medium ? 2 : 5;

  let grouped: number[][] = [];

  for (let i = 0; i < data.length; i = i + modulus) {
    const group = data.slice(i, i + modulus);

    const price = group[0][0];
    const size = group.reduce((acc, curr) => acc + curr[1], 0);

    grouped = [...grouped, [price, size]];
  }

  return grouped;
};

export default coalesceGrouping;

import OrderType from "@enums/OrderType";
import { expect } from "chai";
import Grouping from "../enums/Grouping";
import mapToOrders from "./mapToOrders";

describe("mapToOrders", () => {
  it("Should map & truncate & aggregate total", () => {
    const data: number[][] = [
      [1, 1],
      [2, 2],
      [3, 3],
      [4, 4],
    ];
    const truncate = 2;

    const orders = mapToOrders(data, Grouping.small, OrderType.bid, truncate);

    expect(orders.length).to.equal(truncate);

    expect(orders[0].price).to.equal(data[0][0]);
    expect(orders[0].size).to.equal(data[0][1]);
    expect(orders[0].total).to.equal(data[0][1]);
    expect(orders[0].totalPercentage).to.equal((data[0][1] / 3) * 100);

    expect(orders[1].price).to.equal(data[1][0]);
    expect(orders[1].size).to.equal(data[1][1]);
    expect(orders[1].total).to.equal(data[0][1] + data[1][1]);
    expect(orders[1].totalPercentage).to.equal(100);
  });
});

import OrderType from "@enums/OrderType";
import Grouping from "../enums/Grouping";
import IOrder from "../interfaces/IOrder";
import coalesceGrouping from "./coalesceGrouping";

// note: it is assumed the input data is sorted
const mapToOrders = (
  data: number[][],
  grouping: Grouping,
  type: OrderType,
  truncate: number = 15
): Array<IOrder> => {
  let total = 0;

  data = coalesceGrouping(data, grouping);

  const orders = data.slice(0, truncate).map((data) => {
    total += data[1];

    return {
      type,
      price: data[0],
      size: data[1],
      total,
    };
  });

  return orders.map((order) => {
    return { ...order, totalPercentage: (order.total / total) * 100 };
  });
};

export default mapToOrders;

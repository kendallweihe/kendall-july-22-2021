import IEvent from "../interfaces/IEvent";
import IMessage from "../interfaces/IMessage";
import mergeNewBatch from "./mergeNewBatch";
import { expect } from "chai";
import IData from "../interfaces/IData";
import { defaultRaw } from "../interfaces/IRaw";
import ProductId from "../enums/ProductId";
import Feed from "../enums/Feed";

const zeros: number[][] = [
  [1, 0],
  [2, 0],
  [3, 0],
];

const nonZeros: number[][] = [
  [1, 1],
  [2, 2],
  [3, 3],
];

const mixture: number[][] = [
  [1, 1],
  [2, 0],
  [3, 3],
  [4, 0],
];

const toString = (message: IMessage): string => JSON.stringify(message);

describe("processBatch", () => {
  describe("Snapshot w/ zeros & non-zeros", () => {
    const data: IData = {
      PI_XBTUSD: { ...defaultRaw },
      PI_ETHUSD: { ...defaultRaw },
    };

    const events: Array<IEvent> = [
      {
        data: toString({
          product_id: ProductId.PI_XBTUSD,
          bids: [...mixture],
        }),
      },
    ];

    it("Should not include orders w/ zeros & should include orders with non-zeros", () => {
      const newData = mergeNewBatch(data, events);

      expect(newData.PI_XBTUSD.bids.length).to.equal(2);
      expect(newData.PI_XBTUSD.bids[1][1]).to.not.equal(0);
      expect(newData.PI_XBTUSD.bids[0][1]).to.equal(1);
    });
  });

  describe("New snapshot w/ existing", () => {
    const data: IData = {
      PI_XBTUSD: { ...defaultRaw, bids: [...nonZeros] },
      PI_ETHUSD: { ...defaultRaw },
    };

    const events: Array<IEvent> = [
      {
        data: toString({
          product_id: ProductId.PI_XBTUSD,
          bids: [[4, 4]],
          feed: Feed.book_ui_1_snapshot,
        }),
      },
    ];

    it("Should blank existing and return new snapshot", () => {
      const newData = mergeNewBatch(data, events);

      expect(newData.PI_XBTUSD.bids.length).to.equal(1);
      expect(newData.PI_XBTUSD.bids[0][1]).to.equal(4);
    });
  });

  describe("Delta w/ zeros", () => {
    const data: IData = {
      PI_XBTUSD: { ...defaultRaw, bids: [...nonZeros] },
      PI_ETHUSD: { ...defaultRaw },
    };

    const events: Array<IEvent> = [
      {
        data: toString({ product_id: ProductId.PI_XBTUSD, bids: [...zeros] }),
      },
    ];

    it("Should remove orders w/ zeros", () => {
      const newData = mergeNewBatch(data, events);

      expect(newData.PI_XBTUSD.bids.length).to.equal(0);
    });
  });

  describe("Delta w/ updates", () => {
    const data: IData = {
      PI_XBTUSD: { ...defaultRaw, bids: [...nonZeros] },
      PI_ETHUSD: { ...defaultRaw },
    };

    const newPrice = 2;
    const events: Array<IEvent> = [
      {
        data: toString({
          product_id: ProductId.PI_XBTUSD,
          bids: [[1, newPrice]],
        }),
      },
    ];

    it("Should overwrite existing order", () => {
      const newData = mergeNewBatch(data, events);

      expect(newData.PI_XBTUSD.bids.length).to.equal(3);
      expect(newData.PI_XBTUSD.bids[0][1]).to.equal(newPrice);
    });
  });

  describe("Delta w/ new orders", () => {
    const data: IData = {
      PI_XBTUSD: { ...defaultRaw, bids: [...nonZeros] },
      PI_ETHUSD: { ...defaultRaw },
    };

    const events: Array<IEvent> = [
      {
        data: toString({ product_id: ProductId.PI_XBTUSD, bids: [[4, 4]] }),
      },
    ];

    it("Should include new orders", () => {
      const newData = mergeNewBatch(data, events);

      expect(newData.PI_XBTUSD.bids.length).to.equal(nonZeros.length + 1);
      expect(newData.PI_XBTUSD.bids[3][1]).to.equal(4);
    });
  });
});

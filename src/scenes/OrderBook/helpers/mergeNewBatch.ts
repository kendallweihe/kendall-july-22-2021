import Feed from "../enums/Feed";
import IData from "../interfaces/IData";
import IEvent from "../interfaces/IEvent";
import IMessage from "../interfaces/IMessage";

const mergeNewBatch = (data: IData, events: Array<IEvent>): IData => {
  let newData = { ...data };

  events.forEach((event: IEvent) => {
    const message: IMessage = JSON.parse(event.data);

    if (!message.product_id) return;

    const map = (eventData: number[][], data: number[][]): number[][] => {
      let newData: number[][] = [...data];

      eventData.forEach((dataPoint: number[]) => {
        const exists = data.find((x) => x[0] === dataPoint[0]) !== undefined;

        if (exists) {
          if (dataPoint[1] === 0) {
            // remove from
            newData = [...newData.filter((x) => x[0] !== dataPoint[0])];
          } else if (data.find((x) => (x[0] === dataPoint[0]) !== undefined)) {
            // overwrite existing
            newData = [
              ...newData.map((x) => {
                return x[0] === dataPoint[0] ? dataPoint : x;
              }),
            ];
          }
        } else if (dataPoint[1] !== 0) {
          newData = [...newData, dataPoint];
        }
      });

      return newData;
    };

    if (message.feed === Feed.book_ui_1_snapshot) {
      newData[message.product_id].asks = [];
      newData[message.product_id].bids = [];
    }

    if (message.asks)
      newData[message.product_id].asks = [
        ...map(message.asks, newData[message.product_id].asks),
      ];

    if (message.bids)
      newData[message.product_id].bids = [
        ...map(message.bids, newData[message.product_id].bids),
      ];
  });

  return newData;
};

export default mergeNewBatch;

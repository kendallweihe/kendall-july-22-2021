import OrderType from "@enums/OrderType";
import { useEffect, useMemo, useState } from "react";
import Grouping from "../enums/Grouping";
import ProductId from "../enums/ProductId";
import batch from "../helpers/batch";
import mapToOrders from "../helpers/mapToOrders";
import mergeNewBatch from "../helpers/mergeNewBatch";
import IData, { defaultData } from "../interfaces/IData";
import IEvent from "../interfaces/IEvent";
import IState from "../interfaces/IState";
import useWebSocket from "./useWebSocket";

// TODO unit tests!
const useOrderBook = (): IState => {
  const [data, setData] = useState<IData>({ ...defaultData });
  const [asset, setAsset] = useState<ProductId>(ProductId.PI_XBTUSD);
  const [grouping, setGrouping] = useState<Grouping>(Grouping.small);

  const [events, setEvents] = useState<Array<IEvent>>([]);
  useEffect(() => setData(mergeNewBatch(data, events)), [events]);

  const batchPeriod = 1000;
  const handle = useMemo(() => batch(setEvents, batchPeriod), []);

  const {
    subscribe,
    unsubscribe,
    toggleKill: kill,
    isErrored,
  } = useWebSocket(asset, handle);

  const asks = mapToOrders(
    data[asset].asks.sort((a: number[], b: number[]) => a[0] - b[0]),
    grouping,
    OrderType.ask
  );

  const bids = mapToOrders(
    data[asset].bids.sort((a: number[], b: number[]) => b[0] - a[0]),
    grouping,
    OrderType.bid
  );

  return {
    asks,
    bids,
    asset,
    onClickToggleFeed: () => {
      const newAsset =
        asset === ProductId.PI_XBTUSD
          ? ProductId.PI_ETHUSD
          : ProductId.PI_XBTUSD;

      unsubscribe(asset);
      subscribe(newAsset);

      setAsset(newAsset);
    },
    onChangeGrouping: setGrouping,
    onClickKillFeed: kill,
    isErrored,
    spread: asks[0] && bids[0] ? asks[0].price - bids[0].price : 0,
    spreadPercentage:
      asks[0] && bids[0]
        ? ((asks[0].price - bids[0].price) /
            (bids[0].price + (asks[0].price - bids[0].price) / 2)) *
          100
        : 0,
  };
};

export default useOrderBook;

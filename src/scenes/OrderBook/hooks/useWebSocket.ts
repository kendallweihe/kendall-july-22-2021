import { useEffect, useRef, useState } from "react";
import Event from "../enums/Event";
import Feed from "../enums/Feed";
import ProductId from "../enums/ProductId";
import IEvent from "../interfaces/IEvent";
import IMessage from "../interfaces/IMessage";

// TODO unit tests!
const useWebSocket = (
  asset: ProductId,
  handle: (event: IEvent) => void
): {
  subscribe: (asset: ProductId) => void;
  unsubscribe: (asset: ProductId) => void;
  toggleKill: () => void;
  isErrored: boolean;
} => {
  const socket = useRef<WebSocket>();
  const [isErrored, setIsErrored] = useState<boolean>(false);

  const subscribe = (asset: ProductId) => {
    const message: IMessage = {
      event: Event.subscribe,
      feed: Feed.book_ui_1,
      product_ids: [asset],
    };

    socket.current?.send(JSON.stringify(message));
  };

  const unsubscribe = (asset: ProductId) => {
    const message: IMessage = {
      event: Event.unsubscribe,
      feed: Feed.book_ui_1,
      product_ids: [asset],
    };

    socket.current?.send(JSON.stringify(message));
  };

  const toggleKill = () => {
    if (isErrored) setIsErrored(false);
    else socket.current?.dispatchEvent(new ErrorEvent("error"));
  };

  useEffect(() => {
    socket.current = new WebSocket("wss://www.cryptofacilities.com/ws/v1");

    socket.current?.addEventListener("open", () => subscribe(asset));
    socket.current?.addEventListener("message", handle);
    socket.current?.addEventListener("error", () => setIsErrored(true));

    return () => {
      socket.current?.removeEventListener("open", () => {
        unsubscribe(asset);
        socket.current?.close();
      });
      socket.current?.removeEventListener("message", () => {});
      socket.current?.removeEventListener("error", () => {});
    };
  }, []);

  return {
    subscribe,
    unsubscribe,
    toggleKill,
    isErrored,
  };
};

export default useWebSocket;

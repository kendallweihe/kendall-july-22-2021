import IRaw, { defaultRaw } from "./IRaw";

interface IData {
  PI_XBTUSD: IRaw;
  PI_ETHUSD: IRaw;
}

export const defaultData: IData = {
  PI_XBTUSD: { ...defaultRaw },
  PI_ETHUSD: { ...defaultRaw },
};

export default IData;

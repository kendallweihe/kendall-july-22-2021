interface IEvent {
  data: string;
}

export default IEvent;

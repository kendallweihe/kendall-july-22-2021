import Event from "../enums/Event";
import Feed from "../enums/Feed";
import ProductId from "../enums/ProductId";

interface IMessage {
  event?: Event;
  feed?: Feed;
  product_ids?: Array<ProductId>;
  bids?: number[][];
  asks?: number[][];
  product_id?: ProductId;
}

export default IMessage;

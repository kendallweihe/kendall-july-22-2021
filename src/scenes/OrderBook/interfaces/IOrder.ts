import OrderType from "@enums/OrderType";

interface IOrder {
  type: OrderType;
  price: number;
  size: number;
  total: number;
  totalPercentage: number;
}

export default IOrder;

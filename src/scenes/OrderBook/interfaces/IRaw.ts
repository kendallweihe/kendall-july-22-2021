interface IRaw {
  asks: number[][];
  bids: number[][];
}

export const defaultRaw: IRaw = {
  asks: [],
  bids: [],
};

export default IRaw;

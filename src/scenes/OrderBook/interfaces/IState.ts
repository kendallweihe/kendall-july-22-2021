import Grouping from "../enums/Grouping";
import ProductId from "../enums/ProductId";
import IOrder from "./IOrder";

interface IState {
  asks: Array<IOrder>;
  bids: Array<IOrder>;
  asset: ProductId;
  onClickToggleFeed: () => void;
  onChangeGrouping: (grouping: Grouping) => void;
  onClickKillFeed: () => void;
  isErrored: boolean;
  spread: number;
  spreadPercentage: number;
}

export default IState;

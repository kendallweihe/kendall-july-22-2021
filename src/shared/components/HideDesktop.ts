import styled from "styled-components";

const HideDesktop = styled.div`
  @media only screen and (min-width: 992px) {
    display: none;
  }
`;

export default HideDesktop;

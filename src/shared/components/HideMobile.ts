import styled from "styled-components";

const HideMobile = styled.div`
  @media only screen and (max-width: 991px) {
    display: none;
  }
`;

export default HideMobile;

import styled from "styled-components";

export const Paragraph = styled.p<{
  width?: string | number;
  isCenter?: boolean;
  padding?: string | number;
}>`
  color: ${(props) => props.color};
  margin: 0;
  ${(props) => props.width && `width: ${props.width};`}
  ${(props) => props.isCenter && `text-align: center;`}
  ${(props) => props.padding && `padding: ${props.padding};`}
`;

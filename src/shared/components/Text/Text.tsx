import Color from "@enums/Color";
import { FC } from "react";
import { Paragraph } from "./Text.style";

const Text: FC<{
  color?: Color;
  width?: string | number;
  isCenter?: boolean;
  padding?: string | number;
}> = (props) => {
  return (
    <Paragraph
      color={props.color ?? Color.white}
      width={props.width}
      padding={props.padding}
      isCenter={props.isCenter}
    >
      {props.children}
    </Paragraph>
  );
};

export default Text;

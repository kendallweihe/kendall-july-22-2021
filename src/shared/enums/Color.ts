enum Color {
  white = "white",
  grey = "grey",
  purple = "#5D45DC",
  red = "#ea4d53",
  green = "#65c36e",
  dullRed = "rgba(234, 77, 83, 0.25)",
  dullGreen = "rgba(101, 195, 110, 0.25)",
}

export default Color;

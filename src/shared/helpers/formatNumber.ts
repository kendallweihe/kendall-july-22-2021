// TODO write test
const formatNumber = (value: number, isMoney?: boolean) => {
  let options = {};
  if (isMoney) options = { ...options, style: "currency", currency: "USD" };

  const formatter = new Intl.NumberFormat("en-US", options);

  return formatter.format(value);
};

export default formatNumber;
